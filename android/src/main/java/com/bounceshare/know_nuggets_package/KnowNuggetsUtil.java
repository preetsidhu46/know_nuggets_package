package com.bounceshare.know_nuggets_package;

import android.app.Activity;
import android.app.Notification;
import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.loctoc.knownuggetssdk.KnowNuggetsSDK;
import com.loctoc.knownuggetssdk.modelClasses.KNError;
import com.loctoc.knownuggetssdk.modelClasses.KNInstanceCredentials;
import com.loctoc.knownuggetssdk.modelClasses.newcontent.KnNewContent;

import java.util.ArrayList;
import java.util.Map;

import io.flutter.plugin.common.MethodChannel;


public class KnowNuggetsUtil {

    private static KnowNuggetsUtil knowNuggetsUtil;
    private static Activity activity;
    private KNInstanceCredentials knInstanceCredentials;
    private NotificationManagerCompat notificationManager;

    public static KnowNuggetsUtil getInstance(Activity context) {
        if (knowNuggetsUtil==null){
            knowNuggetsUtil= new KnowNuggetsUtil();
            activity=context;
        }

        return knowNuggetsUtil;
    }

    public KNInstanceCredentials getCredentials(Context context){
        Log.e("knInstanceCredentials",""+knInstanceCredentials);
        if (knInstanceCredentials==null) {
            knInstanceCredentials = new KNInstanceCredentials();
            knInstanceCredentials.setApiKey(context.getString(R.string.know_nuggets_api_key));
            knInstanceCredentials.setDatabaseUrl(context.getString(R.string.know_nuggets_db_url));
            knInstanceCredentials.setAppId(context.getString(R.string.know_nuggets_app_id));
            knInstanceCredentials.setStorageBucket(context.getString(R.string.know_nuggets_strorage_bucket));
        }
        return knInstanceCredentials;
    }

    public void initSDK(Context context){
        // Initialize sdk
        KnowNuggetsSDK.getInstance().initKNSDK(context, getCredentials(context));
        provideNotification(context);
    }

    public void initApp(Context context,IKNAppInit callback){

        // Initialize app
        KnowNuggetsSDK.getInstance().initKNApp(new KnowNuggetsSDK.KNAppInit() {
            @Override
            public void onSuccess() {
                callback.onKNAppInitSuccess();
            }
            @Override
            public void onFailure(KNError error) {
                Log.d("KnowNuggetsSDK", "" + error.getErrorCode());
                callback.onKNAppInitFailure();
            }
        },context);
    }

    public void provideNotification(Context context){
        notificationManager= NotificationManagerCompat.from(context);

        KnowNuggetsSDK.getInstance().setListenerForNewContent(context,
                new KnowNuggetsSDK.KNResult<KnNewContent>() {
                    @Override
                    public void onSuccess(KnNewContent newContent) {
                        if (newContent != null) {
// See the following switch statement for all possible nugge types.
                                    String nuggetType = (String) newContent.getType();
                            String nuggetTitle = (String) newContent.getTitle();
                            String nuggetID = (String) newContent.getNuggetId();
                            String authorName = (String) newContent.getAuthorName();
                            long createdAt = newContent.getCreatedAt();
                            switch (nuggetType) {
                                case KnowNuggetsSDK.KnContentType.KN_GENERAL_NUGGET:
                                    Log.e("notification", "general");
                                    Notification n=getNotification(context, "New Announcement", "You have a new announcement created by "+authorName);
                                    notificationManager.notify(1, n);
                                    KnowNuggetsPlugin.notificationArrived();
                                    break;
                                case KnowNuggetsSDK.KnContentType.KN_TODO_NUGGET:
                                    Log.e("notification", "my to do");
                                    break;
                                case KnowNuggetsSDK.KnContentType.KN_LEARN_NUGGET:
                                    Log.e("notification", "my learn nugget");
                                    break;
                                case KnowNuggetsSDK.KnContentType.KN_COURSE:// User received a New Course nugget
                                    Log.e("notification", "my coarse");
                                    Notification noti=getNotification(context, "New Course", "You have a new course created by "+authorName);
                                    notificationManager.notify(1, noti);
                                    KnowNuggetsPlugin.notificationArrived();
                                    break;
                                case KnowNuggetsSDK.KnContentType.KN_NEW_FORM:
                                    Log.e("notification", "my new form");
                                    break;
                                default:
                                    break;
                            }
                        }
                    }

                    @Override
                    public void onFailure(KNError knError) {
                        Log.e("failiure notification", "Notification send failed");
                    }
                });
    }

    public void userAuthenticated(Map userDetails, MethodChannel.Result result){
        knowNuggetsUtil.authenticateUser(activity, (String) userDetails.get("agentId"), new IKNUserAuthentication() {
            @Override
            public void onKNSuccess() {
               Log.d("onSuccess","" +
                        "");
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("tagging", "hhhh tag");
                        knowNuggetsUtil.updateUserName(activity,(String) userDetails.get("userName"),(String) userDetails.get("userName"));

                    }
                },500);
                Handler handler1 = new Handler();
                handler1.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        ArrayList<String> userDetailList =new ArrayList<>();
                        userDetailList.add((String)userDetails.get("mobileNumber"));
                        userDetailList.add((String)userDetails.get("agentId"));
                         KnowNuggetsSDK.getInstance().addTags(activity, userDetailList);

                    }
                },1000);
                result.success(true);
            }

            @Override
            public void onKNFailure() {
                result.success(false);
            }
        });
    }

    public void authenticateUserUtil(Map userDetails, MethodChannel.Result result){
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                knowNuggetsUtil.updateUserName(activity,(String) userDetails.get("userName"),(String)userDetails.get("agentID"));

            }
        },500);
        ArrayList<String> userDetailList = new ArrayList<>();
        userDetailList.add((String)userDetails.get("mobileNumber"));
        userDetailList.add((String)userDetails.get("agentId"));

        KnowNuggetsSDK.getInstance().setTags(activity, userDetailList);
        result.success(true);
    }

    private Notification getNotification(Context context, String title, String body) {
        NotificationManagerCompat notificationManager= NotificationManagerCompat.from(context);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Notification.Builder builder = new Notification.Builder(context, MethodChannels.NOTIFICATION_CHANNEL_ID)
                    .setContentText(body).setContentTitle(title).setSmallIcon(R.mipmap.notification).setColor(ContextCompat.getColor(context, R.color.red))
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE);
            return builder.build();
        }else{
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context, MethodChannels.NOTIFICATION_CHANNEL_ID)
                    .setContentText(body).setContentTitle(title).setSmallIcon(R.mipmap.notification).setColor(ContextCompat.getColor(context, R.color.red))
                    .setCategory(NotificationCompat.CATEGORY_MESSAGE).setPriority(NotificationCompat.PRIORITY_HIGH);
            return builder.build();
        }
    }

    public boolean isUserAuthenticated(Context context){
        return (KnowNuggetsSDK.getInstance().isUserAuthenticated(context,
                getCredentials(context)));
    }

    public void authenticateUser(Activity context, String userIdentifier, IKNUserAuthentication callBacks){

        KnowNuggetsSDK.getInstance().authenticateUser(context, context.getString(R.string.know_nuggets_authentication_key), userIdentifier,
                new KnowNuggetsSDK.SdkAuthCallBacks() {
                    @Override
                    public void onSdkAuthSuccess() {
                        // knowNuggetsUtil.updateUserName(activity,"Nishant","SINGH");
                        callBacks.onKNSuccess();
                    }
                    @Override
                    public void onSdkAuthFailure(KNError knError) {
                        knError.getErrorCode(); //Error Code
                        Log.e("onSdkAuthFailure",""+knError.getErrorMessage()); //Error Message
                        callBacks.onKNFailure();

                    }
                });
    }

    public void updateUserName(Activity context,String firstName,String secondName){
        KnowNuggetsSDK.getInstance().updateName(context, firstName,secondName);
      //  KnowNuggetsSDK.getInstance().
    }
}
