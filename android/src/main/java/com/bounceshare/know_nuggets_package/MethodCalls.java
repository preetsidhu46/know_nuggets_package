package com.bounceshare.know_nuggets_package;

public class MethodCalls {

    public interface KnowNuggets{
        String OPEN_KNOW_NUGGETS_HOME = "knowNuggetHome";
        String INIT_KNOW_NUGGETS = "initKnowNugget";
        String AUTHENTICATE_KNOW_NUGGETS_USER = "userAuthenticationKnowNugget";
        String UPDATE_USER_NAME = "userNameUpdateKnowNugget";
    }

}
