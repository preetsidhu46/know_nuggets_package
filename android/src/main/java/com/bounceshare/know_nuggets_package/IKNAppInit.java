package com.bounceshare.know_nuggets_package;

public interface IKNAppInit {
    void onKNAppInitSuccess();
    void onKNAppInitFailure();
}
