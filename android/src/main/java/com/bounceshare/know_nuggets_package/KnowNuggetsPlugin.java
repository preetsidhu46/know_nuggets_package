package com.bounceshare.know_nuggets_package;


import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import java.util.Map;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;


public class KnowNuggetsPlugin implements MethodChannel.MethodCallHandler {

    static MethodChannel methodChannel;

    static Activity activity;
    static KnowNuggetsUtil knowNuggetsUtil;

    public static void registerWith(Registrar registrar) {
        methodChannel = new MethodChannel(registrar.messenger(), MethodChannels.KNOW_NUGGETS_CHANNEL);
        methodChannel.setMethodCallHandler(new KnowNuggetsPlugin());
        activity = registrar.activity();
        initSDK();
    }

    private static void initSDK() {
        knowNuggetsUtil=  KnowNuggetsUtil.getInstance(activity);

    }


    @Override
    public void onMethodCall(MethodCall methodCallName, MethodChannel.Result result) {
        String methodCall = methodCallName.method.trim();
        switch (methodCall) {
            case MethodCalls.KnowNuggets.OPEN_KNOW_NUGGETS_HOME:
                Log.d("OPEN_KNOW_NUGGETS_HOME", "======");
                Intent intent= new Intent(activity, KnowNuggetsHome.class);
                activity.startActivity(intent);
                result.success(true);
                break;
            case MethodCalls.KnowNuggets.INIT_KNOW_NUGGETS:

              knowNuggetsUtil.initSDK(activity);
              knowNuggetsUtil.initApp(activity, new IKNAppInit() {
                  @Override
                  public void onKNAppInitSuccess() {
                      Log.d("onSuccess","onKNAppInitSuccess");
                      result.success(true);
                  }

                  @Override
                  public void onKNAppInitFailure() {
                      Log.d("onFailure","onKNAppInitFailure");
                      result.success(false);

                  }
              });
                break;
            case MethodCalls.KnowNuggets.AUTHENTICATE_KNOW_NUGGETS_USER:
                Log.d("authentit", String.valueOf(knowNuggetsUtil.isUserAuthenticated(activity)));
                Map userDetails = (Map) methodCallName.arguments;
                Log.e("userDetails1",""+userDetails.toString());

                if (!knowNuggetsUtil.isUserAuthenticated(activity)){
                    knowNuggetsUtil.userAuthenticated(userDetails, result);
                 } else{
                     // Map userDetails = (Map) methodCallName.arguments;
                    knowNuggetsUtil.authenticateUserUtil(userDetails, result);
                 }
                break;

            case MethodCalls.KnowNuggets.UPDATE_USER_NAME:
                if (knowNuggetsUtil.isUserAuthenticated(activity)){
                    Map userDetailsRender = (Map) methodCallName.arguments;
                    Log.e("userDetails",""+userDetailsRender.toString());
                    knowNuggetsUtil.updateUserName(activity,(String) userDetailsRender.get("userName"),(String)userDetailsRender.get("agentID"));
                    result.success(true);
                }
            break;
            default:
                result.notImplemented();
        }
    }

    public static void notificationArrived(){
        try{
            if(methodChannel!=null){
                Log.e("notification", "hhhhhhh");
                methodChannel.invokeMethod("knNotificationArrived",null);
            }
        }catch (IllegalStateException e){

        }
    }

}
