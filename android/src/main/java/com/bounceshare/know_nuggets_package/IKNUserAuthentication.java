package com.bounceshare.know_nuggets_package;

public interface IKNUserAuthentication {
    void onKNSuccess();
    void onKNFailure();
}
