package com.bounceshare.know_nuggets_package;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.loctoc.knownuggetssdk.modelClasses.feed.Feed;
import com.loctoc.knownuggetssdk.views.feed.RecyclerViewFeed;

public class KnowNuggetsHome extends AppCompatActivity implements RecyclerViewFeed.OnFeedInteractionListener {

    TabLayout tabLayout;
    TabItem tabAnnouncements;
    TabItem tabTraining;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_know_nuggets_home);

        tabLayout = findViewById(R.id.tablayout);
        tabTraining = findViewById(R.id.training);
        viewPager = findViewById(R.id.viewPager);
        tabAnnouncements = findViewById(R.id.announcements);
        Log.e("tabwwww", String.valueOf((tabLayout.getTabCount())));
        PageAdapter pageAdapter = new PageAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pageAdapter);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
                Log.e("tabChange", String.valueOf(tab.getPosition()));
                switch(tab.getPosition()) {
                    case 0:
                        new announcement();
                    case 1:
                        new training();
                    default:
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));




    }

    @Override
    public void nuggetLoaded() {

    }

    @Override
    public void onShareClicked(Feed feed, int i, boolean b) {

    }
}
