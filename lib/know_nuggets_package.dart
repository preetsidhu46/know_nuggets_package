import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

class KnowNuggetsPackage {
  static const MethodChannel _channel =
      MethodChannel('com.hawkeye.knownuggets');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  Function callbackMethod;
  KnowNuggetsPackage({@required Function callback}) {
    callbackMethod = callback;
    _channel.setMethodCallHandler((m) => setupMethodHandler(m));
  }

  setupMethodHandler(m) async {
    String channelName = m.method.trim();
    print("channelName =====> $channelName");
    switch (channelName) {
      case "knNotificationArrived":
        print('fdfdfd');
        callbackMethod(methodName: channelName);
        break;
    }
  }

  openKnowNuggetHome() async {
    bool status = await _channel.invokeMethod("knowNuggetHome");
    print('openKnowNuggetHome==>>> $status');
  }

  knNotificationArrived() {
    print('fdfdfdrrrrrrrrrrr');
  }

  initKnowNuggets() async {
    return await _channel.invokeMethod("initKnowNugget");
  }

  authenticateKnowNuggetUser({@required Map userData}) async {
    return await _channel.invokeMethod(
        "userAuthenticationKnowNugget", userData);
  }

  updateknowNuggetUserName({@required Map userData}) async {
    return await _channel.invokeMethod("userNameUpdateKnowNugget", userData);
  }
}
