import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:know_nuggets_package/know_nuggets_package.dart';

void main() {
  const MethodChannel channel = MethodChannel('know_nuggets_package');

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await KnowNuggetsPackage.platformVersion, '42');
  });
}
