#import "KnowNuggetsPackagePlugin.h"
#import <know_nuggets_package/know_nuggets_package-Swift.h>

@implementation KnowNuggetsPackagePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftKnowNuggetsPackagePlugin registerWithRegistrar:registrar];
}
@end
